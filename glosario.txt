  Matias Sepulveda Miles
  19.940.257-9

  Definiciones:

 Control de versiones distribuido: En programación informática, el control de versiones distribuido permite a muchos desarrolladores de software trabajar en un proyecto común sin necesidad de compartir una misma red.

 git pull: Comando de git que actualiza tu repositorio local al commit mas nuevo

 git push: Es un comando que sube los cambios hechos en tu ambiente de trabajo a una rama de trabajo tuya y/o de tu equipo remota. 

 git clone: Comando usado para copiar repositorios existentes.

 git checkout: se usa para cambiar de ramas.
